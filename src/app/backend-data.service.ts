import { Injectable } from '@angular/core';
import {HttpClient} from '@angular/common/http';

@Injectable({
  providedIn: 'root'
})
export class BackendDataService {
  static isLoggedIn = false;
  private rootUrl = 'https://cl.dakshindia.org';

  constructor(private http: HttpClient) { }

  login() {
    return this.http.post(this.rootUrl + '/v2/court-log/login',
      JSON.stringify({user_name: 'puneetpahwa@dakshindia.org', password: 'daksh123', device_uuid: 'null'}),
      {headers: {'Content-Type': 'application/json'}});
  }

  getMetadata() {
    if (BackendDataService.isLoggedIn) {
      return this.http.get(this.rootUrl + '/v1/metadata');
    } else {
      throw Error('No user logged in! Please call login() on an instance of BackendDataService first.');
    }
  }

  getObjectMetadata(object: string) {
    if (BackendDataService.isLoggedIn) {
      return this.http.get(this.rootUrl + '/v1/metadata/' + object);
    } else {
      throw Error('No user logged in! Please call login() on an instance of BackendDataService first.');
    }
  }
}

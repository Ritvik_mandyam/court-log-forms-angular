import {Component, EventEmitter, forwardRef, Input, OnInit, Output, ViewChild} from '@angular/core';
import {Option} from '../option';
import {
  AbstractControl,
  ControlValueAccessor,
  NG_VALIDATORS,
  NG_VALUE_ACCESSOR,
  NgModel,
  ValidationErrors,
  Validator
} from '@angular/forms';
import * as moment from 'moment';

@Component({
  selector: 'app-form-field',
  templateUrl: './form-field.component.html',
  styleUrls: ['./form-field.component.sass'],
  providers: [
    {provide: NG_VALUE_ACCESSOR, useExisting: forwardRef(() => FormFieldComponent), multi: true},
    {provide: NG_VALIDATORS, useExisting: FormFieldComponent, multi: true},
    ]
})
export class FormFieldComponent implements OnInit, ControlValueAccessor, Validator {
  private innerValue: any;
  private changed = [];
  private touched = [];
  private disabled: boolean;

  @Input() fieldType: string;
  @Input() options: Option[];
  @Input() name: string;

  @Output() valueChanged = new EventEmitter();

  @ViewChild(NgModel) model: NgModel;

  constructor() { }

  ngOnInit() {
  }

  get value(): any {
    return this.innerValue;
  }


  set value(value: any) {
    if (this.innerValue !== value) {
      this.innerValue = value;
      this.changed.forEach(f => f(value));
      this.valueChanged.emit();
    }
  }

  set date(date: any) {
    if (this.innerValue !== date) {
      let currentTime;
      if (this.innerValue !== undefined) {
        currentTime = this.innerValue.getTime();
      }
      this.innerValue = date;
      if (currentTime) {
        this.innerValue.setTime(currentTime);
      }
      this.changed.forEach(f => f(this.innerValue));
      this.valueChanged.emit();
    }
  }

  get date(): any {
    if (this.innerValue instanceof Date) {
      return this.innerValue.getDate();
    }
  }

  set time(time: any) {
    if (this.innerValue === undefined) {
      this.innerValue = new Date();
    }
    if (moment(this.innerValue.getTime(), 'HH:mm') !== time) {
      const timeValues = time.split(':');
      this.innerValue.setHours(timeValues[0]);
      this.innerValue.setMinutes(timeValues[1]);
      this.changed.forEach(f => f(this.innerValue));
      this.valueChanged.emit();
    }
  }

  get time(): any {
    if (this.innerValue instanceof Date) {
      return this.innerValue.getTime();
    }
  }

  writeValue(value: any) {
    this.innerValue = value;
  }


  registerOnChange(fn: any) {
    this.changed.push(fn);
  }

  setDisabledState(isDisabled: boolean): void {
    this.disabled = isDisabled;
  }

  registerOnTouched(fn: any) {
    this.touched.push(fn);
  }

  validate(control: AbstractControl): ValidationErrors {
    if ((this.fieldType === 'integer' || this.fieldType === 'bigint') && !Number.isInteger(this.value)) {
      return {'isNotIntegerError': {value: control.value}};
    } else if (this.fieldType === 'char' && this.value && this.value.length > 1) {
      return {'isNotCharError': {value: control.value}};
    } else {
      return null;
    }
  }
}

import {Component, OnInit, ViewChild} from '@angular/core';
import {NgForm} from '@angular/forms';
import {BackendDataService} from './backend-data.service';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.sass']
})
export class AppComponent implements OnInit {
  @ViewChild(NgForm) form: NgForm;
  formObjects = [];

  constructor(private dataService: BackendDataService) {}

  ngOnInit(): void {
      this.dataService.login()
        .subscribe((loginResponse) => {
          if (loginResponse['status'] === 'ok') {
            BackendDataService.isLoggedIn = true;
            this.dataService.getObjectMetadata('case')
              .subscribe((data) => {
                this.formObjects = data['tables']['columns'];
              });
          }
        });
  }

  onSubmit(value): void {
    alert(`Submit: ${JSON.stringify(value)}`);
  }
}

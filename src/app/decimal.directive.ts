import { Directive } from '@angular/core';
import {AbstractControl, NG_VALIDATORS, ValidationErrors, Validator} from '@angular/forms';

@Directive({
  selector: '[appDecimal]',
  providers: [{provide: NG_VALIDATORS, useExisting: DecimalDirective, multi: true}]
})
export class DecimalDirective implements Validator {
  constructor() { }

  validate(control: AbstractControl): ValidationErrors | null {
    if (!Number.isInteger(control.value)) {
      return {'notAnInteger': {value: control.value}};
    } else {
      return null;
    }
  }
}
